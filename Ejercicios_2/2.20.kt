/* AUTHOR: Adrian Galinsoga Egea
* DATE: 29/09/22
* TITLE: Exercicis UF1 2.20
*
*
*/


import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)
    val unidadPeso = scanner.next()
    val peso = scanner.nextDouble()




    if (unidadPeso == "G" || unidadPeso == "g") {
        print(peso / 10000000)
        println(" TN")
        print(peso / 1000)
        println(" KG")


    } else if (unidadPeso == "KG" || unidadPeso == "kg") {
        print((peso * 1000).toInt())
        println(" G")
        print((peso / 1000).toInt())
        println(" TN")

    } else if (unidadPeso == "TN" || unidadPeso == "tn") {
        print(peso * 1e+6)
        println(" G")
        print(peso * 1000)
        println(" KG")
    }
    else {
        println("Te has equivocado, vuelve a intentar")
    }

}