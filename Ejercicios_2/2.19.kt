/* AUTHOR: Adrian Galinsoga Egea
* DATE: 29/09/22
* TITLE: Exercicis UF1 2.19
*
*
*/


import java.util.*
fun main() {


    val scanner = Scanner(System.`in`)
    val nota = scanner.nextDouble()



    if (nota in 1.0 .. 4.0){
        println("Insuficient")

    } else if (nota in 4.0 .. 5.0) {
        println("Suficient")

    } else if (nota in 5.0 .. 6.0) {
        println("Bé")

    } else if (nota in 6.0 .. 8.0) {
        println("Notable")

    } else if (nota in 8.0 .. 10.0 ) {
        println("Exel·lent")
    }
}

