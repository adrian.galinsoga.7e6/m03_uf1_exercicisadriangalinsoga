/* AUTHOR: Adrian Galinsoga Egea
* DATE: 04/11/22
* TITLE: Examen UF1 Part B
* 1. Programa que obté el quocient i la resta de dos nombres sencers positius mitjançant restes.
*
*/

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    var num = scanner.nextInt()
    var resultat = 0

    for(i in 1..num) {
        resultat += i
    }
        println(resultat)

}