/* AUTHOR: Adrian Galinsoga Egea
* DATE: 04/11/22
* TITLE: Examen UF1 Part B
* 1. Programa que obté el quocient i la resta de dos nombres sencers positius mitjançant restes.
*
*/
import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    var dividendo = scanner.nextInt()
    val divisor = scanner.nextInt()

    var cociente = 0

    while (dividendo > 0) {
        cociente++
        dividendo -= divisor
    }
    println("Cociente " + cociente)
    println("Resto " + (-dividendo))


}
