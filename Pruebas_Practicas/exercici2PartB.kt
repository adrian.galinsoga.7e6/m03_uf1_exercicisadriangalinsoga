/* AUTHOR: Adrian Galinsoga Egea
* DATE: 04/11/22
* TITLE: Examen UF1 Part B
* 2. Escriu un programa que demana a l’usuari la seva data de naixement i treu per pantalla el missatge: “És veritat/mentida que estàs jubilat" (estan jubilats els que tenen 65 o més anys).
*
*/
import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    val years = scanner.nextInt()

    if (years >= 65) {
        print("Es veritat están Jubilats")
    } else {
        println("Es mentida están jubilats")
    }
}