/* AUTHOR: Adrian Galinsoga Egea
* DATE: 04/11/22
* TITLE: Examen UF1 Part B
* 3. Programa que demana dos valors numèrics a l’usuari (x i N) i calcula la mitjana harmònica i la mitja quàntica:
*
*/
import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()
    var harmonicMean = 0.0
    var quadraticMean = 0.0
    for (i in 1..n) {
        harmonicMean += 1.0/ i
        quadraticMean += i*i
    }
println("$n/harmonicMean")
    println("$sqrt(quadraticMean/n")

}