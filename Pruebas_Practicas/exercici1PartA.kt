/* AUTHOR: Adrian Galinsoga Egea
* DATE: 04/11/22
* TITLE: Examen UF1 Part B
* 1. Programa que obté el quocient i la resta de dos nombres sencers positius mitjançant restes.
*
*/
import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    var firstFactor = scanner.nextInt()
    var secondFactor = scanner.nextInt()
    var result = 0


    while (secondFactor > 0) {
        if (secondFactor%2 == 1) result += firstFactor
       firstFactor*= 2
        secondFactor /=2
    }

    println("The result is $result")


}
