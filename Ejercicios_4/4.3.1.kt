/* AUTHOR: Adrian Galinsoga Egea
* DATE: 7/10/22
* TITLE: Exercicis UF1 4.2
*
*
*/

fun main(args: Array<String>) {

    var resultado=0.0

    for (arg in args) {
        var suma=arg.toDouble()
        resultado+=suma
    }

    print (resultado/args.size)

}