
/* AUTHOR: Adrian Galinsoga Egea
* DATE: 7/10/22
* TITLE: Exercicis UF1 4.1
*
*
*/



fun removeDuplicates(array: IntArray): IntArray {
    return array.distinct().toIntArray()
}
fun main(args: Array<String>) {

    val array: IntArray = intArrayOf(23, 23, 4, 4, 67, 78, 89, 4)


    val distinct = removeDuplicates(array)

    println(distinct.contentToString())
}



}