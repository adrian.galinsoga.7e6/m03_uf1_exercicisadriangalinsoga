/* AUTHOR: Adrian Galinsoga Egea
* DATE: 29/09/22
* TITLE: Exercicis UF1 2.5
*
*
*/
import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    val primervalor = scanner.nextInt()
    val segundovalor = scanner.nextInt()

    val operacion = scanner.next().single()


    when (operacion)
    {
        '+'-> println(primervalor+segundovalor)
        '-'->println(primervalor-segundovalor)
        '*'->println(primervalor*segundovalor)
        '/'->println(primervalor/segundovalor)
        '%'->println(primervalor+segundovalor)
    }

}


