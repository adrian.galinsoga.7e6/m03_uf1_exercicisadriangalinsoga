/* AUTHOR: Adrian Galinsoga Egea
* DATE: 22/09/19
* TITLE: Exercicis UF1 1.8
*
*
*/
import java.util.*
fun main() {

// LEER UN NOMBRE DECIMAL
    val scanner = Scanner(System.`in`)
    println("Introdueix un nªdecimal")

// Cambiamos el anterior nextInt por el nextDouble al pedirnos en el ejercicio que sea un numero decimal
    val numero = scanner.nextDouble()


// RESULTADO

    println("Resultado numero Decimal")
    println(numero * 2)

}