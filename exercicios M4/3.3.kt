/* AUTHOR: Adrian Galinsoga Egea
* DATE: 7/10/22
* TITLE: Exercicis UF1 3.3
*
*
*/
import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    val a = scanner.nextInt()
    val b = scanner.nextInt()

    for(it in a..b) {
        print (",$it")
    }

}