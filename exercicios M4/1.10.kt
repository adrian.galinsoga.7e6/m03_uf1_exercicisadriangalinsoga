/* AUTHOR: Adrian Galinsoga Egea
* DATE: 22/09/19
* TITLE: Exercicis UF1 1.10
*
*
*/
import java.util.*
fun main() {

// LEER EL DIAMETRO DE UNA PIZZA
    val scanner = Scanner(System.`in`)
    println("Introdueix el diàmetre d'una pizza")

// LEE EL diametro
    val diametro = scanner.nextDouble()

// RESULTADO (TAMBIÉN PUEDE SER EN VEZ DE Match.Pi CON 3.14)

    println("Diàmetre de la pizza")
    println(diametro/2*diametro/2*Math.PI)



}