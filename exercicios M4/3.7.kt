

/* AUTHOR: Adrian Galinsoga Egea
* DATE: 22/09/19
* TITLE: Exercicis UF1 1.8
*
*
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix numero enter")

    var number = scanner.nextInt()

    var invertido = 0

    while (number>0) {
        var resto = number % 10
        invertido = invertido * 10 + resto
        number /= 10

    }
    println("Num Inv. $invertido")


}