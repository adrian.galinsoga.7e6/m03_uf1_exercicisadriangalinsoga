/* AUTHOR: Adrian Galinsoga Egea
* DATE: 29/09/22
* TITLE: Exercicis UF1 2.5
*
*
*/
import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)
    println("Introduce tu número de DNI: ")
    val numDNI = scanner.nextInt()



    val operacion = numDNI % 23


    when (operacion)
    {
    0 -> println ("$numDNI T")
    1 -> println ("$numDNI R")
    2 -> println ("$numDNI W")
    3 -> println ("$numDNI A")
    4 -> println ("$numDNI G")
    5 -> println ("$numDNI M")
    6 -> println ("$numDNI Y")
    7 -> println ("$numDNI F")
    8 -> println ("$numDNI P")
    9 -> println ("$numDNI D")
    10 -> println ("$numDNI X")
    11 -> println ("$numDNI B")
    12 -> println ("$numDNI N")
    13 -> println ("$numDNI J")
    14 -> println ("$numDNI Z")
    15 -> println ("$numDNI S")
    16 -> println ("$numDNI Q")
    17 -> println ("$numDNI V")
    18 -> println ("$numDNI H")
    19 -> println ("$numDNI L")
    20 -> println ("$numDNI C")
    21 -> println ("$numDNI K")
    22 -> println ("$numDNI E")

}

}


