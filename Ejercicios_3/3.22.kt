
/* AUTHOR: Adrian Galinsoga Egea
* DATE: 7/10/22
* TITLE: Exercicis UF1 3.6
*
*
*/
import java.util.*
import java.lang.Math.abs

fun main() {

    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()
 

    for(i in 1 until 2*n){
val espais = abs(n:n-i)
val asteriscos = if (2*i - 1 < 2*n) 2*i-1
		else (2*i-1)-(abs(n:n-1)*4)
repeat (espais) {print("")}
repeat (asteriscos) {print(*)}
println()
    }



}