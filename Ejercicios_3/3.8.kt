/* AUTHOR: Adrian Galinsoga Egea
* DATE: 22/09/19
* TITLE: Exercicis UF1 3.8
*
*
*/
import java.util.*
fun main() {

    val scanner = Scanner(System.`in`)
    println("Introdueix un nombre enter")

    var numero = scanner.nextInt()
    var cont=0

    while (numero !=0){
        numero/=10
        ++cont
    }
    println("Digitos $cont")


}