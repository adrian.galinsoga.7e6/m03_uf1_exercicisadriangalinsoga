/* AUTHOR: Adrian Galinsoga Egea
* DATE: 7/10/22
* TITLE: Exercicis UF1 3.1
*
*
*/
import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    val a = scanner.nextInt()
    var resultat = 0

    for(it in 1..a) {
        resultat += it
    }

    println(resultat)
}